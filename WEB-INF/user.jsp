<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import ="java.util.ArrayList"%>
<%@ page import="sample.User"%>
<html>
  <head>
    <style>
      table{ width: 100%; border-spacing: 0; }
      table th{ border-bottom: solid 2px #fb5144; padding: 10px 0; }
      table td{ border-bottom: solid 2px #ddd; text-align: center; padding: 10px 0; }
    </style>
  </head>
<% ArrayList<User> userlist = new ArrayList<User>(); %>
<%  userlist = (ArrayList<User>)request.getAttribute("users"); %>
  <body>
  <h3>User's list for sample application.</h3>
    <table>
      <tr>
        <th>ID</th>
        <th>NICK NAME</th>
        <th>FIRST NAME</th>
        <th>LAST NAME</th>
        <th>E-MAIL</th>
        <th>LAST LOGIN</th>
      </tr>
       <% for(User user : userlist) { %>
      <tr>
         <td><%= user.getId() %></td>
         <td><%= user.getNickname() %></td>
         <td><%= user.getFirstName() %></td>
         <td><%= user.getLastName() %></td>
         <td><%= user.getMail() %></td>
         <td><%= user.getDate() %></td>
       </tr>
       <% } %>
     </table>
  </body>
</html>
