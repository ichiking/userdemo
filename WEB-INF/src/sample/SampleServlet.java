package sample;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.WebServlet;

@WebServlet("/users")
public class SampleServlet extends HttpServlet {
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
  throws ServletException, IOException {

    UserModel model = new UserModel();
    ArrayList<User> list = model.getUsers();

    request.setAttribute("users", list);
    RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/user.jsp");
    dispatcher.forward(request, response);
  }
}
