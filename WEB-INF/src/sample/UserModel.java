package sample;

import java.sql.*;
import java.util.*;

public class UserModel {
  private String url = "jdbc:mysql://localhost:3306/neo01";
  private String SQL = "select * from users";
  private ArrayList<User> list = new ArrayList<>();
  private User user;

  public ArrayList<User> getUsers(){
    try {
      Class.forName("com.mysql.jdbc.Driver");
      Connection conn = DriverManager.getConnection(url, "user01", "user01");
      PreparedStatement ps = conn.prepareStatement(SQL);
      ResultSet rs = ps.executeQuery();

      while (rs.next()) {
        user = new User();
        user.setId(rs.getInt("user_id"));
        user.setNickname(rs.getString("nickname"));
        user.setPass(rs.getString("password"));
        user.setFirstName(rs.getString("first_name"));
        user.setLastName(rs.getString("last_name"));
        user.setMail(rs.getString("email"));
        user.setDate(rs.getString("updated_at"));
        list.add(user);
      }
      conn.close();
    }catch(Exception e){
      e.printStackTrace();
    }

    return list;
  }
}
